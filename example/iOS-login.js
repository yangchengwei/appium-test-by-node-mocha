"use strict";

require("./helpers/setup");

var caps = require("./helpers/caps");


var wd = require("wd"),
    _ = require('underscore'),
    Q = require('q'),
    serverConfigs = require('./helpers/appium-servers');

describe("ios simple", function () {
  this.timeout(300000);
  var driver;
  var allPassed = true;

  before(function () {
    var serverConfig = process.env.SAUCE ?
      serverConfigs.sauce : serverConfigs.local;
    driver = wd.promiseChainRemote(serverConfig);
    require("./helpers/logging").configure(driver);

    var desired = _.clone(caps).ios81;
    //desired.app = require("./helpers/apps").iosTestApp;
    desired.app = "/Users/zhangchi/Desktop/virallender/DerivedData/yqrx/Build/Products/Debug-iphonesimulator/友钱任信.app";

    if (process.env.SAUCE) {
      desired.name = 'ios - simple';
      desired.tags = ['sample'];
    }
    return driver.init(desired);
  });

  after(function () {
    // return driver
    //   .quit()
    //   .finally(function () {
    //     if (process.env.SAUCE) {
    //       return driver.sauceJobStatus(allPassed);
    //     }
    //   });
    return driver.sauceJobStatus(allPassed);
  });

  afterEach(function () {
    allPassed = allPassed && this.currentTest.state === 'passed';
  });

  function textLogin(phoneNum,pwd) {
    var seq = _(['phone', 'password']).map(function (name) {
      return function (sum) {
        return driver.waitForElementByName(name, 3000).then(function (el) {
          var x = (name == 'phone')? phoneNum: pwd;
          return el.type(''+ x).then(function () { return sum; }).sleep(3000);
        }).then(function () { return sum; }); // dismissing keyboard;
      };
    });
    return seq.reduce(Q.when, new Q(0));
   // driver.elementByXPath("//UIAApplication[1]/UIAWindow[1]/UIAElement[1]/UIATextField[1]").type(13333333)

  }


    it("loginError", function () {
        return driver
        .resolve(textLogin(13333333333,123))
        .then(function () {
            driver.elementByXPath("//UIAApplication[1]/UIAWindow[1]/UIAButton[1]").click().sleep(3000);
            if (driver.elementByXPath("//UIAApplication[1]/UIAWindow[1]/UIAAlert[1]/UIAScrollView[1]/UIAStaticText[2]").text().should.become("请输入 6-12 位密码")) {
                return driver.elementByXPath("//UIAApplication[1]/UIAWindow[1]/UIAAlert[1]/UIACollectionView[1]/UIACollectionCell[1]").click().sleep(3000);
            }
            else if (driver.elementByXPath("//UIAApplication[1]/UIAWindow[1]/UIAAlert[1]/UIAScrollView[1]/UIAStaticText[2]").text().should.become("手机号未注册")) {
                return driver.elementByXPath("//UIAApplication[1]/UIAWindow[1]/UIAAlert[1]/UIACollectionView[1]/UIACollectionCell[1]").click().sleep(3000);
            }
        });
    });
    it("loginSuccess", function () {
        return driver
        .resolve(textLogin(15812345672,"12345a"))
        .then(function () {
            return driver.elementByXPath("//UIAApplication[1]/UIAWindow[1]/UIAButton[1]").click().sleep(3000).elementByXPath("//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAStaticText[1]")
        .text().should.become("友钱任信");
        });
    });
    
    
});
   
    
