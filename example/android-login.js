"use strict";

require("./helpers/setup");

var wd = require("wd"),
    _ = require('underscore'),
    Q = require('q'),
    serverConfigs = require('./helpers/appium-servers');

describe("android login", function () {
  this.timeout(300000);
  var driver;
  var allPassed = true;

  before(function () {
    var serverConfig = serverConfigs.local;
    driver = wd.promiseChainRemote(serverConfig);
    require("./helpers/logging").configure(driver);

    var desired = _.clone(require("./helpers/caps").android19);
    desired.app = "/Users/apple/Desktop/SampleTest/youqianrenxin-HJS-debug.apk";
    return driver.init(desired);
  });

  after(function () {
    return driver
      .quit()
  });

  afterEach(function () {
    allPassed = allPassed && this.currentTest.state === 'passed';
  });

  function loginInput(tag) {
    var seq = _(['phonenumber', 'password']).map(function (name) {
      return function () {
        return driver.waitForElementByName(name, 3000).then(function (el) {
          var x;
          switch (tag) {
            case 0:  //不是有效的手机号
              x = (name == 'phonenumber') ?'1371814765':'123456t';
              break;
            case 1: //手机号或密码不正确
              x = (name == 'phonenumber') ?'13718147657':'123456y';
              break;
            case 2:  //手机号未注册
              x = (name == 'phonenumber') ?'13718154768':'123456t';
              break;
            case 3:  //成功登录
              x = (name == 'phonenumber') ?'13718147657':'123456t';
              break;
          }
          return el.type(''+x)
          });
      };
    });
    return seq.reduce(Q.when, new Q(0));
  }

 function resetInput(){
   var seq = _(['phonenumber','newpassword','surepassword']).map(function (name) {
     return function () {
       return driver.waitForElementByName(name, 3000).then(function (el) {
         var x;
         x = (name == 'phonenumber') ? '13718147657':'123456y'
         return el.type(''+x)
         });
     };
   });
   return seq.reduce(Q.when, new Q(0));
 }

  function clickYes(){
    return driver.
      elementByXPath('//android.widget.TextView[@text=\'是\']')
      .click().sleep(1000);
  }

  it("login phone number error", function () {
    return driver
      .resolve(loginInput(0)).then(function () {
        return driver.
          elementByAccessibilityId('login')
            .click().sleep(1000)
              .elementByXPath('//android.widget.TextView').text().should.become("不是有效手机号");
      })
  });
  it("click '是' button",function(){
    return driver.
      elementByXPath('//android.widget.TextView[@text=\'是\']')
      .click().sleep(1000);
  })

  it("login phone don't register", function () {
    return driver
      .resolve(loginInput(2)).then(function () {
        return driver.
          elementByAccessibilityId('login')
            .click().sleep(1000)
            .elementByXPath('//android.widget.TextView').text().should.become("手机号未注册");
      })
  });
  it("click '是' button",function(){
    return driver.
      elementByXPath('//android.widget.TextView[@text=\'是\']')
      .click().sleep(1000);
  })
  it("login password error", function () {
    return driver
      .resolve(loginInput(1)).then(function () {
        return driver.
          elementByAccessibilityId('login')
            .click().sleep(1000)
            .elementByXPath('//android.widget.TextView').text().should.become("用户名或密码错误");
      })
  });
  it("click '是' button",function(){
    return driver.
      elementByXPath('//android.widget.TextView[@text=\'是\']')
      .click().sleep(1000);
  })

  it("login success", function () {
    return driver
      .resolve(loginInput(3)).then(function () {
        return driver.
          elementByAccessibilityId('login').click().sleep(1000)
          .elementByXPath('//android.widget.TextView').text().should.become("友钱任信");
      })

  });

});
